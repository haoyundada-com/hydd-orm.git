<?php

// +----------------------------------------------------------------------
// | HaoyundadaWordpress [ WE CAN DO IT JUST HAOYUNDADA ]
// +----------------------------------------------------------------------
// | Copyright (c) 2024~2024 http://haoyundada.com All rights reserved.
// +----------------------------------------------------------------------
// | @Remind        : 使用盗版主题会存在各种未知风险。支持正版，从我做起！
// +----------------------------------------------------------------------
// | Author: THX <53297668@qq.com>
// +----------------------------------------------------------------------
declare(strict_types=1);

namespace haoyundada\db;

use Stringable;

/**
 * SQL Raw.
 */
class Raw
{
    /**
     * 创建一个查询表达式.
     *
     * @param string|Stringable $value
     * @param array  $bind
     *
     * @return void
     */
    public function __construct(protected string|Stringable $value, protected array $bind = [])
    {
    }

    /**
     * 获取表达式.
     *
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * 获取参数绑定.
     *
     * @return array
     */
    public function getBind(): array
    {
        return $this->bind;
    }
}
