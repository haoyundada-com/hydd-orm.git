<?php

// +----------------------------------------------------------------------
// | HaoyundadaWordpress [ WE CAN DO IT JUST HAOYUNDADA ]
// +----------------------------------------------------------------------
// | Copyright (c) 2024~2024 http://haoyundada.com All rights reserved.
// +----------------------------------------------------------------------
// | @Remind        : 使用盗版主题会存在各种未知风险。支持正版，从我做起！
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://zjzit.cn>
// +----------------------------------------------------------------------
declare(strict_types=1);

namespace haoyundada\db\exception;

/**
 * PDO异常处理类
 * 重新封装了系统的\PDOException类.
 */
class PDOException extends DbException
{
    /**
     * PDOException constructor.
     *
     * @param \PDOException $exception
     * @param array         $config
     * @param string        $sql
     * @param int           $code
     */
    public function __construct(\PDOException $exception, array $config = [], string $sql = '', int $code = 10501)
    {
        $error = $exception->errorInfo;
        $message = $exception->getMessage();

        if (!empty($error)) {
            $this->setData('PDO Error Info', [
                'SQLSTATE'             => $error[0],
                'Driver Error Code'    => $error[1] ?? 0,
                'Driver Error Message' => $error[2] ?? '',
            ]);
        }

        parent::__construct($message, $config, $sql, $code);
    }
}
