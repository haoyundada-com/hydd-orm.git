<?php

declare(strict_types=1);

namespace haoyundada\model\contracts;

use haoyundada\Model;

interface FieldTypeTransform
{
    public static function get(mixed $value, Model $model): static;

    /**
     * @return static|mixed
     */
    public static function set($value, Model $model): mixed;
}
